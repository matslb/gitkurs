<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Git demo</title>
    <style media="screen">
    body{
      background: red;
    }
  
    .content{
        display: flex;
        flex-wrap: wrap;
      }
    @media screen and (max-width: 768px){
      .content{
        flex-direction: column;
      }
    }
    h1{
      background: blue;
    }
    h2{
      background: brown;
    }
    .sidebar{
      background: green;
      width: 30%;
      min-width: 19rem;
      padding: 1rem;
    }
    main{
      width: 60%;
      padding: 1rem;
    }
    </style>
  </head>
  <body>
